#!/usr/bin/env python
# coding: utf-8


import pytest
import verification
from shapely.geometry import Polygon,MultiPolygon


cloud = Polygon([[3000,1000],[2000,2000],[2000,3000],[3000,4000],[4000,4000],[5000,3000],
                 [5000,2000],[4000,1000],[3000,1000]])

forecast1E1 = MultiPolygon([Polygon([[2000,0],[2000,2000],[4000,2000],[4000,0],[2000,0]]),
                  Polygon([[2000,3000],[3000,5000],[4000,3000],[2000,3000]])])

forecast1E2 = MultiPolygon([Polygon([[4000,1000],[4000,4000],[6000,4000],[6000,1000],[4000,1000]]),
                  Polygon([[2000,0],[2000,2000],[4000,2000],[4000,0],[2000,0]]),Polygon([[3000,1000],[1000,2000],
                                                                                         [3000,4000],[3000,1000]])])

forecast2E1 = Polygon([])
forecast2E2 = MultiPolygon([Polygon([[2000,2000],[4000,6000],[5000,2000],[2000,2000]]),
                  Polygon([[2000,0],[2000,2000],[4000,2000],[4000,0],[2000,0]])])

all_forecast = []
ensemble = []
ensemble.append(forecast1E1)
ensemble.append(forecast1E2)
all_forecast.append(ensemble)
ensemble = []
ensemble.append(forecast2E1)
ensemble.append(forecast2E2)
all_forecast.append(ensemble)
    
def test_accuracy():
    assert(verification.verification(cloud,all_forecast[0][0],
                                     'AC').verif_skill() == 0.5003174949731244)

def test_threat_score():
    assert(verification.verification(cloud,all_forecast[0][1],
                        'TS').verif_skill() == 0.3514300894271335)

def test_equitable_threat_score():
    assert(verification.verification(cloud,all_forecast[1][1],
                        'ETS').verif_skill() == 0.26722462631384375)

def test_true_skill_statistic():
    assert(verification.verification(cloud,all_forecast[1][1],
                        'TSS').verif_skill() == 0.45459523691522524)

def test_cauchy_dist():
    assert(verification.verification(cloud,all_forecast[0][1],
                        'CD').verif_skill() == 1.0807550135156779e-07)







